/**      _           __    _        __  ___
 *      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 *  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 * |___/    https://gitlab.com/joakimaling    /___/
 *
 * main.cpp
 * 
 * (c) 2022 Joakim Åling
 * This code is released under GPLv3 licence. See LICENSE.md for more.
 */

#include <allegro5/allegro_native_dialog.h>
#include <core/core.hpp>

using namespace circuit;

int main() {
	try {
		auto& core = Core::getInstance();
		return core.run();
	}
	catch (std::runtime_error const& error) {
		al_show_native_message_box(
			al_get_current_display(),
			"Error",
			"An error has occurred!",
			error.what(),
			"Quit",
			ALLEGRO_MESSAGEBOX_ERROR
		);
		return 1;
	}
}
