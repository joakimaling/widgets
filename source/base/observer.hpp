/**     _           __    _        __  ___
 *     (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *    / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 * __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 *|___/    https://gitlab.com/joakimaling    /___/
 *
 * base/observer.hpp
 *
 * Observer Design Pattern.
 */

#pragma once

#include <list>

class Observer {
	public:
		virtual ~Observer() = default;

		virtual void update() = 0;
};

class Subject {
	private:
		std::list<Observer*> observers;

	public:
		virtual ~Subject() = default;

		void attach(Observer& observer) {
			observers.push_back(&observer);
		}

		void detach(Observer& observer) {
			observers.remove(&observer);
		}

		void notify() {
			for (auto const& observer: observers) {
				observer->update();
			}
		}
};
