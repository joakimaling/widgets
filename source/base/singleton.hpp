/**      _           __    _        __  ___
 *      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 *  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 * |___/    https://gitlab.com/joakimaling    /___/
 *
 * base/singleton.hpp
 * 
 * Singleton Design Pattern.
 */

#pragma once

namespace base {
	template<class Type>
	class Singleton {
		protected:
			Singleton() = default;
			virtual ~Singleton() = default;

		public:
			// Scott Meyers mentions in his Effective Modern C++ book, that
			// deleted functions should generally be public as it results in
			// better error messages. This is due to compilers' behaviour to
			// check accessibility before deleted status
			Singleton(Singleton const&) = delete;
			Singleton(Singleton&&) = delete;
			Singleton& operator=(Singleton const&) = delete;
			Singleton& operator=(Singleton&&) = delete;

			static Type& getInstance() {
				static Type instance;
				return instance;
			}
	};
}
