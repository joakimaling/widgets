/**      _           __    _        __  ___
 *      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 *  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 * |___/    https://gitlab.com/joakimaling    /___/
 *
 * core/component.cpp
 * 
 * (c) 2022 Joakim Åling
 * This code is released under GPLv3 licence. See LICENSE.md for more.
 */

#include <core/component.hpp>

namespace circuit {
	Component::Component(std::string const& name = "") {
		if (!(font = al_load_font("resources/arial.ttf", 18, 0))) {
			throw std::runtime_error("Couldn't load the font");
		}

		height = al_get_font_line_height(font);
		move(300, 200);
		set_name(name);
	}

	Component::~Component() {
		al_destroy_font(font);
	}

	std::string const& Component::get_name() const {
		return name;
	}

	void Component::move(float const x, float const y) {
		this->x = x;
		this->y = y;
	}

	void Component::set_name(std::string const& name) {
		this->name = name;
		this->width = al_get_text_width(font, name.c_str());
	}
}
