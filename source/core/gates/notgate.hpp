/**      _           __    _        __  ___
 *      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 *  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 * |___/    https://gitlab.com/joakimaling    /___/
 *
 * core/andgate.hpp
 * 
 * (c) 2022 Joakim Åling
 * This code is released under GPLv3 licence. See LICENSE.md for more.
 */

#pragma once

#include <core/chip.hpp>

namespace circuit {
	class NotGate: public Chip<1, 1> {
		using Chip<1, 1>::inputs;
		using Chip<1, 1>::outputs;

		protected:
			bool compute() const {
				return !inputs[0];
			}

		public:
			NotGate(): Chip<1, 1>{"Not"} {}
	};
}
