/**      _           __    _        __  ___
 *      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 *  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 * |___/    https://gitlab.com/joakimaling    /___/
 *
 * core/core.cpp
 * 
 * (c) 2022 Joakim Åling
 * This code is released under GPLv3 licence. See LICENSE.md for more.
 */

#include <core/core.hpp>

namespace circuit {
	Core::Core() {
		// Try to initialise Allegro
		if (!al_init()) {
			throw std::runtime_error("Couldn't initialise Allegro");
		}

		al_get_monitor_info(3, &monitor);

		// Calculate the position of the window to centre it on the monitor
		uint32_t const x = (-width + monitor.x1 + monitor.x2) / 2;
		uint32_t const y = (-height + monitor.y1 + monitor.y2) / 2;

		// Set position of window to be used on creation
		al_set_new_display_flags(ALLEGRO_WINDOWED);
		al_set_new_window_position(x, y);

		// Try to create the display
		if (!(display = al_create_display(width, height))) {
			throw std::runtime_error("Couldn't create the display");
		}

		// Try to create the event queue
		if (!(event_queue = al_create_event_queue())) {
			throw std::runtime_error("Couldn't create the event queue");
		}

		// Try to create the timer
		if (!(timer = al_create_timer(1.0 / 60.0))) {
			throw std::runtime_error("Couldn't create the timer");
		}

		// Try to set up the keyboard
		if (!al_install_keyboard()) {
			throw std::runtime_error("Couldn't set up the keyboard");
		}

		// Try to set up the mouse
		if (!al_install_mouse()) {
			throw std::runtime_error("Couldn't set up the mouse");
		}

		// Try to set up the primitives
		if (!al_init_primitives_addon()) {
			throw std::runtime_error("Couldn't initialise the primitives addon");
		}

		// Try to set up True Type fonts
		if (!al_init_font_addon() || !al_init_ttf_addon()) {
			throw std::runtime_error("Couldn't initialise the font addons");
		}

		// Load monospace font
		if (!(monospace = al_load_ttf_font("resources/LiberationMono-Regular.ttf", 12, 0))) {
			throw std::runtime_error("Couldn't load the font");
		}

		// Load sans serif font
		if (!(sans_serif = al_load_ttf_font("resources/LiberationSans-Regular.ttf", 12, 0))) {
			throw std::runtime_error("Couldn't load the font");
		}

		// Try to, temporarily, set up the image addon
		if (!al_init_image_addon()) {
			throw std::runtime_error("Couldn't initialise the image addon");
		}

		// Load the icon for the application
		if (!(bitmap = al_load_bitmap("resources/icon.png"))) {
			throw std::runtime_error("Couldn't load the icon");
		}

		// Shut down the image addon since it's not needed anymore
		al_shutdown_image_addon();

		// Register event sources
		al_register_event_source(event_queue, al_get_display_event_source(display));
		al_register_event_source(event_queue, al_get_keyboard_event_source());
		al_register_event_source(event_queue, al_get_mouse_event_source());
		al_register_event_source(event_queue, al_get_timer_event_source(timer));

		// Set fancy window attributes
		al_set_display_icon(display, bitmap);
		al_set_window_title(display, "Circuits");

		// Adjust graphics to scale properly
		al_identity_transform(&transform);
		al_scale_transform(&transform, scale, scale);
		al_use_transform(&transform);

		// Start the global timer
		al_start_timer(timer);

		// Test: Add components
		components.push_back(new AndGate());
	}

	Core::~Core() {
		// Test: Delete components
		for (auto const* component: components) {
			delete component;
		}

		al_destroy_bitmap(bitmap);
		al_destroy_display(display);
		al_destroy_event_queue(event_queue);
		al_destroy_font(monospace);
		al_destroy_font(sans_serif);
		al_destroy_timer(timer);
		al_uninstall_system();
	}

	void Core::draw() {
		al_clear_to_color(al_map_rgb(128, 0, 128));

		// Test: Draw components
		for (auto const& component: components) {
			component->draw();
		}

		// Collects some data for debugging purposes
		al_get_mouse_state(&mouse);

		// Then draws an overlay using these data
		al_draw_multiline_textf(
			monospace,
			al_map_rgb(255, 255, 255),
			10,
			10,
			200,
			20,
			0,
			"Mouse:% 5d,% 5d\n",
			mouse.x < 0 ? 0 : mouse.x,
			mouse.y < 0 ? 0 : mouse.y
		);

		al_flip_display();
	}

	uint8_t Core::run() {
		while (true) {
			al_wait_for_event(event_queue, &event);

			switch (event.type) {
				case ALLEGRO_EVENT_DISPLAY_CLOSE:
					return 0;

				case ALLEGRO_EVENT_TIMER:
					redraw = true;
			}

			if (redraw && al_is_event_queue_empty(event_queue)) {
				redraw = false;
				draw();
			}
		}

		return 0;
	}

	void Core::tick() {

	}
}
