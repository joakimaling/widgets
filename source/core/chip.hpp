/**      _           __    _        __  ___
 *      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 *  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 * |___/    https://gitlab.com/joakimaling    /___/
 *
 * core/gate.hpp
 * 
 * (c) 2022 Joakim Åling
 * This code is released under GPLv3 licence. See LICENSE.md for more.
 */

#pragma once

#include <allegro5/allegro_primitives.h>
#include <core/component.hpp>
#include <array>

namespace circuit {
	template <uint8_t IN, uint8_t OUT>
	class Chip: public Component {
		protected:
			std::array<bool, IN> inputs;
			std::array<bool, OUT> outputs;

			bool state = false;

			/**
			 * Computes the current state based on the state of the inputs.
			 */
			virtual bool compute() const = 0;

			void notify() {
				for (auto const& output: outputs) {
					output.update();
				}
			}

			void set_state(bool const& state) {
				this->state = state;
			}

			void update() {
				if (get_state() != set_state(compute())) {
					notify();
				}
			}

		public:
			Chip(std::string const& name = "Untitled"): Component{name} {}
			virtual ~Chip() = default;

			void draw() {
				al_draw_filled_rectangle(x, y, x + width, y + height, al_map_rgb(255, 0, 0));
				al_draw_text(font, al_map_rgb(0, 0, 0), x, y, ALLEGRO_ALIGN_LEFT, name.c_str());

				for (uint8_t input = 0; input < inputs.size(); input++) {
					al_draw_filled_circle(x, y, 2, al_map_rgb(0, 0, 255));
				}

				for (uint8_t output = 0; output < outputs.size(); output++) {
					al_draw_filled_circle(x + width, y, 2, al_map_rgb(0, 0, 255));
				}
			}

			bool const& get_state() const {
				return state;
			}
	};
}
