/**      _           __    _        __  ___
 *      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 *  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 * |___/    https://gitlab.com/joakimaling    /___/
 *
 * core/core.hpp
 * 
 * (c) 2022 Joakim Åling
 * This code is released under GPLv3 licence. See LICENSE.md for more.
 */

#pragma once

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include <base/singleton.hpp>
#include <core/component.hpp>
#include <core/gates/andgate.hpp>
#include <stdexcept>
#include <string>
#include <vector>

namespace circuit {
	class Core: public base::Singleton<Core> {
		private:
			ALLEGRO_BITMAP* bitmap;
			ALLEGRO_DISPLAY* display;
			ALLEGRO_EVENT event;
			ALLEGRO_FONT* monospace;
			ALLEGRO_FONT* sans_serif;
			ALLEGRO_EVENT_QUEUE* event_queue;
			ALLEGRO_MONITOR_INFO monitor;
			ALLEGRO_MOUSE_STATE mouse;
			ALLEGRO_TIMER* timer;
			ALLEGRO_TRANSFORM transform;

			// Window properties
			float const scale = 2.0f;
			uint32_t const width = 640 * scale;
			uint32_t const height = width / 16 * 9;

			// Game state
			std::vector<Component*> components;
			bool redraw = false;

			/**
			 *
			 */
			void draw();

			/**
			 *
			 */
			void tick();

		public:
			Core();
			~Core();

			/**
			 *
			 */
			uint8_t run();
	};
}
