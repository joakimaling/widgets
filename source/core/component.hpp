/**      _           __    _        __  ___
 *      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
 *     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
 *  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
 * |___/    https://gitlab.com/joakimaling    /___/
 *
 * core/component.hpp
 * 
 * (c) 2022 Joakim Åling
 * This code is released under GPLv3 licence. See LICENSE.md for more.
 */

#pragma once

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <stdexcept>
#include <string>

namespace circuit {
	class Component {
		protected:
			ALLEGRO_FONT* font;
			uint32_t height, width;
			std::string name;
			float x, y;

		public:
			Component(std::string const&);
			virtual ~Component();

			/**
			 *
			 */
			virtual void draw() = 0;

			/**
			 *
			 */
			std::string const& get_name() const;

			/**
			 *
			 */
			void move(float const, float const);

			/**
			 *
			 */
			void set_name(std::string const&);
	};
}
