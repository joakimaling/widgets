#       _           __    _        __  ___
#      (_)__  ___ _/ /__ (_)_ _  _(())/ (_)__  ___ _
#     / / _ \/ _ `/  '_// /  ' \/ _ `/ / / _ \/ _ `/
#  __/ /\___/\_,_/_/\_\/_/_/_/_/\_,_/_/_/_//_/\_, /
# |___/    https://gitlab.com/joakimaling    /___/
#

# Compiler & linker flags
CXXFLAGS := -MMD -MP -Wall -Wextra -pedantic -std=c++11
LDFLAGS := $(shell pkg-config --libs allegro_{dialog,image,primitives,ttf}-5)

# Directories
build := ./build
release := ./release
resources := ./resources
source := ./source

# Include source code
INCLUDE := -I$(source)

# Find all objects & dependencies
sources := $(shell find $(source) -name '*.cpp')
objects := $(sources:$(source)/%.cpp=$(build)/%.o)
depends := $(objects:%.o=%.d)

# Name of target
target := $(build)/circuits

# Declare non-file targets
.PHONY: all build class clean debug release run tags

# Run all tasks
all: build $(target)

# Link objects into target
$(target): $(objects)
	$(CXX) $(LDFLAGS) -o $@ $^

# Compile source code & create dependencies
$(build)/%.o: $(source)/%.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c -o $@ $<

# Include dependencies
-include $(depends)

# Create & populate build directory
build:
	@mkdir -p $(dir $(objects))

# Create class
class:
	(cd $(source) && create-class -L GPLv3 -i -l C++)

# Remove dependencies, objects & target
clean:
	-@rm -frv $(build)

# Compile debuggable code & run debugger
debug: CXXFLAGS += -DDEBUG -Og -g
debug: clean all
	gdb $(target)

# Compile target optimised for release
release: CXXFLAGS += -DNDEBUG -O3
release: clean all
	@mkdir -p $(resources)
	@cp $(resources)/* $(release)
	@cp $(target) $(release)

# Run target
run: all
	@./$(target)

# Create tags
tags:
	ctags -R  $(source)
